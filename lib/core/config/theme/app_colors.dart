import 'package:flutter/material.dart';

Map<int, Color> _primaryColorMap = {
  50: const Color.fromRGBO(20, 151, 255, .1),
  100: const Color.fromRGBO(20, 151, 255, .2),
  200: const Color.fromRGBO(20, 151, 255, .3),
  300: const Color.fromRGBO(20, 151, 255, .4),
  400: const Color.fromRGBO(20, 151, 255, .5),
  500: const Color.fromRGBO(20, 151, 255, .6),
  600: const Color.fromRGBO(20, 151, 255, .7),
  700: const Color.fromRGBO(20, 151, 255, .8),
  800: const Color.fromRGBO(20, 151, 255, .9),
  900: const Color.fromRGBO(22, 152, 255, 1.0),
};
MaterialColor _materialColorPrimary = MaterialColor(0xff6896bb, _primaryColorMap);

class AppColors {
  static MaterialColor primary = _materialColorPrimary;
  static const Color primaryDark = Color(0xff00cbfa);
  static const Color secondary = Color(0xffFCAE02);
  static const Color secondaryLight = Color(0xffDDCD83);
  static const Color third = Color(0xffC3CEBF);
  static const Color pageBackground = Color(0xffFBF8F3);

  static const Color black = Colors.black;
  static const Color white = Colors.white;
  static const Color grey = Colors.grey;
  static const Color success = Colors.green;
  static const Color error = Colors.red;
  static const Color whiteSmoke = Color(0xfff3f3f3);
  static const Color blackGrey = Color(0xff676464);
  static const Color buttonColor = Color(0xffe30714);
}
