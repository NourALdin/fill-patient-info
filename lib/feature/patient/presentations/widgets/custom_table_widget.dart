import 'package:flutter/material.dart';
import 'package:medical_project/core/config/theme/app_colors.dart';
import 'package:medical_project/core/extensions/media_query.dart';
import 'package:medical_project/core/extensions/string_extensions.dart';
import 'package:medical_project/core/feature/data/model/base_model.dart';
import 'package:medical_project/core/feature/presentation/widgets/custom_stream_builder.dart';
import 'package:responsive_table/responsive_table.dart';
import 'package:rxdart/rxdart.dart';

class CustomTableWidget<T extends BaseTableModel> extends StatefulWidget {
  const CustomTableWidget(
      {Key? key,
      required this.data,
      this.actions,
      this.topOfTableWidget,
      this.newEvent /*, required this.bloc*/,
      this.onTapRow,
      this.prefixTitleWhenTableIsEmpty})
      : super(key: key);
  final T data;
  final Row? Function(dynamic id)? actions;
  final Widget? topOfTableWidget;
  final Function? newEvent;
  final Function(Map<String, dynamic> data)? onTapRow;
  final List<Widget>? prefixTitleWhenTableIsEmpty;

  @override
  State<CustomTableWidget> createState() => _CustomTableWidgetState();
}

class _CustomTableWidgetState extends State<CustomTableWidget> with CustomTableMixin {
  _initializeData() async {
    await _mockPullData();
  }

  _mockPullData() async {
    _expanded = List.generate(_currentPerPage!, (index) => false);

    changeLoadingState(true);
    Future.delayed(const Duration(milliseconds: 100)).then((value) {
      _sourceOriginal.clear();
      _sourceOriginal.addAll(widget.data.toDataTable());
      _sourceFiltered = _sourceOriginal;
      _total = _sourceFiltered.length;
      _source = _sourceFiltered.getRange(0, _currentPerPage! > _sourceOriginal.length ? _sourceOriginal.length : _currentPerPage!).toList();
      changeLoadingState(false);
    });
  }

  _resetData({start = 0}) async {
    changeLoadingState(true);
    var expandedLen = _total - start < _currentPerPage! ? _total - start : _currentPerPage;
    Future.delayed(const Duration(seconds: 0)).then((value) {
      _expanded = List.generate(expandedLen as int, (index) => false);
      _source.clear();
      _source = _sourceFiltered.getRange(start, start + expandedLen).toList();
      changeLoadingState(false);
    });
  }

  _filterData(value) {
    changeLoadingState(true);
    try {
      if (value == "" || value == null) {
        _sourceFiltered = _sourceOriginal;
      } else {
        _sourceFiltered = _sourceOriginal
            .where((dataFiltered) =>
                dataFiltered['id'].toString().toLowerCase().contains(value.toString().toLowerCase()) ||
                dataFiltered['name'].toString().toLowerCase().contains(value.toString().toLowerCase()) ||
                dataFiltered['phoneNumber'].toString().toLowerCase().contains(value.toString().toLowerCase()))
            .toList();
      }

      _total = _sourceFiltered.length;
      var rangeTop = _total < _currentPerPage! ? _total : _currentPerPage!;
      _expanded = List.generate(rangeTop, (index) => false);
      _source = _sourceFiltered.getRange(0, rangeTop).toList();
    } catch (e) {
      // Logger().e(e);
    }
    changeLoadingState(false);
  }

  @override
  void initState() {
    super.initState();

    for (var element in widget.data.getTableKeys) {
      _headers.add(DatatableHeader(text: element.getTitleFromTableKey(), value: element, show: true, sortable: true, textAlign: TextAlign.center));
    }
    if (widget.actions != null) {
      _headers.add(DatatableHeader(
        text: "Action",
        value: 'action',
        show: true,
        sortable: false,
        textAlign: TextAlign.center,
        sourceBuilder: (value, row) {
          return widget.actions!(row['id'])!;
        },
      ));
    }
    _initializeData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: context.width * 0.025, vertical: context.height * 0.025),
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.max, children: [
          Container(
            margin: const EdgeInsets.all(10),
            padding: const EdgeInsets.all(0),
            constraints: const BoxConstraints(
              maxHeight: 650,
            ),
            child: Card(
              elevation: 10,
              shadowColor: Colors.black,
              clipBehavior: Clip.none,
              color: AppColors.blackGrey,
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                    width: 1.5,
                    color: AppColors.primary,
                  ),
                  borderRadius: BorderRadius.circular(8)),
              child: Column(
                children: [
                  ...[widget.topOfTableWidget ?? const SizedBox.shrink()],
                  Flexible(
                      flex: 9,
                      child: CustomStreamBuilder<bool>(
                          stream: _isLoading.stream,
                          onData: (snapshot) {
                            return Stack(
                              children: [
                                ResponsiveDatatable(
                                  title: TextButton.icon(
                                    onPressed: () {
                                      widget.newEvent?.call();
                                    },
                                    icon: const Icon(
                                      Icons.add,
                                      color: AppColors.white,
                                      size: 20,
                                    ),
                                    label: const Text(
                                      "سجل جديد",
                                      style: TextStyle(color: AppColors.white, fontSize: 17, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  actions: widget.data.toDataTable().isEmpty
                                      ? widget.prefixTitleWhenTableIsEmpty
                                      : [
                                          if (_isSearch) _buildSearchWidget(),
                                          if (!_isSearch)
                                            IconButton(
                                                icon: const Icon(
                                                  Icons.search,
                                                  color: AppColors.whiteSmoke,
                                                ),
                                                onPressed: () {
                                                  setState(() {
                                                    _isSearch = true;
                                                  });
                                                })
                                        ],
                                  headers: widget.data.toDataTable().isEmpty ? [] : _headers,
                                  source: widget.data.toDataTable().isEmpty ? [] : _source,
                                  selecteds: _selecteds,
                                  headerDecoration: BoxDecoration(
                                    color: AppColors.primary,
                                  ),
                                  hideUnderline: false,
                                  isExpandRows: true,
                                  autoHeight: false,
                                  onChangedRow: (value, header) {},
                                  onSubmittedRow: (value, header) {},
                                  onTabRow: widget.onTapRow,
                                  onSort: (value) {
                                    changeLoadingState(true);
                                    setState(() {
                                      _sortColumn = value;
                                      _sortAscending = !_sortAscending;
                                      if (_sortAscending) {
                                        _sourceFiltered.sort((a, b) => b["$_sortColumn"].compareTo(a["$_sortColumn"]));
                                      } else {
                                        _sourceFiltered.sort((a, b) => a["$_sortColumn"].compareTo(b["$_sortColumn"]));
                                      }
                                      var rangeTop = _currentPerPage! < _sourceFiltered.length ? _currentPerPage! : _sourceFiltered.length;
                                      _source = _sourceFiltered.getRange(0, rangeTop).toList();
                                      // _searchKey = value;

                                      changeLoadingState(false);
                                    });
                                  },
                                  expanded: _expanded,
                                  sortAscending: _sortAscending,
                                  sortColumn: _sortColumn,
                                  isLoading: snapshot.data!,
                                  onSelect: (value, item) {
                                    if (value!) {
                                      setState(() => _selecteds.add(item));
                                    } else {
                                      setState(() => _selecteds.removeAt(_selecteds.indexOf(item)));
                                    }
                                  },
                                  onSelectAll: (value) {
                                    if (value!) {
                                      setState(() => _selecteds = _source.map((entry) => entry).toList().cast());
                                    } else {
                                      setState(() => _selecteds.clear());
                                    }
                                  },
                                  rowTextStyle:
                                      TextStyle(color: Theme.of(context).brightness == Brightness.dark ? AppColors.whiteSmoke : AppColors.white, fontSize: 14),
                                  headerTextStyle: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                  footers: widget.data.toDataTable().isEmpty
                                      ? []
                                      : [
                                          Container(
                                            padding: const EdgeInsets.symmetric(horizontal: 15),
                                            child: const Text(
                                              "عدد السجلات للصفحة الواحدة:",
                                              style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white70,
                                              ),
                                            ),
                                          ),
                                          if (_perPages.isNotEmpty)
                                            Container(
                                              padding: const EdgeInsets.symmetric(horizontal: 15),
                                              child: DropdownButton<int>(
                                                style: TextStyle(
                                                  color: AppColors.white,
                                                  backgroundColor: AppColors.primary,
                                                ),
                                                value: _currentPerPage,
                                                items: _perPages
                                                    .map((e) => DropdownMenuItem<int>(
                                                          value: e,
                                                          child: Text("$e"),
                                                        ))
                                                    .toList(),
                                                onChanged: (dynamic value) {
                                                  setState(() {
                                                    _currentPerPage = value;
                                                    _currentPage = 1;
                                                    _resetData();
                                                  });
                                                },
                                                isExpanded: false,
                                              ),
                                            ),
                                          Container(
                                            padding: const EdgeInsets.symmetric(horizontal: 15),
                                            child: Text(
                                              "$_currentPage - $_currentPerPage من $_total",
                                              style: const TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white70,
                                              ),
                                            ),
                                          ),
                                          IconButton(
                                            icon: const Icon(
                                              Icons.arrow_back_ios,
                                              color: Colors.white70,
                                              size: 16,
                                            ),
                                            onPressed: _currentPage == 1
                                                ? null
                                                : () {
                                                    var nextSet0 = _currentPage - _currentPerPage!;
                                                    setState(() {
                                                      _currentPage = nextSet0 > 1 ? nextSet0 : 1;
                                                      _resetData(start: _currentPage - 1);
                                                    });
                                                  },
                                            padding: const EdgeInsets.symmetric(horizontal: 15),
                                          ),
                                          IconButton(
                                            icon: const Icon(
                                              Icons.arrow_forward_ios,
                                              size: 16,
                                              color: Colors.white70,
                                            ),
                                            onPressed: _currentPage + _currentPerPage! - 1 > _total
                                                ? null
                                                : () {
                                                    var nextSet = _currentPage + _currentPerPage!;

                                                    setState(() {
                                                      _currentPage = nextSet < _total ? nextSet : _total - _currentPerPage!;
                                                      _resetData(start: nextSet - 1);
                                                    });
                                                  },
                                            padding: const EdgeInsets.symmetric(horizontal: 15),
                                          )
                                        ],
                                ),
                                if (widget.data.toDataTable().isEmpty)
                                  const Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "ليست هناك اي بيانات",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  )
                              ],
                            );
                          },
                          onError: Container())),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  _buildSearchWidget() {
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 12,
        vertical: 5,
      ),
      child: TextField(
        textAlign: TextAlign.center,
        style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
        decoration: InputDecoration(
          hintText: 'أدخل عبارة البحث',
          hintStyle: const TextStyle(color: Colors.white54),
          prefixIcon: IconButton(
              icon: const Icon(
                Icons.cancel,
                color: AppColors.third,
              ),
              onPressed: () {
                setState(() {
                  _isSearch = false;
                });
                _initializeData();
              }),
          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(12), borderSide: const BorderSide(color: Colors.white, width: 2)),
          suffixIcon: IconButton(
              icon: const Icon(
                Icons.search,
                color: AppColors.third,
              ),
              onPressed: () {}),
        ),
        onChanged: (value) {
          _filterData(value);
        },
      ),
    ));
  }
}

mixin CustomTableMixin {
  final List<DatatableHeader> _headers = [];

  final List<int> _perPages = [10, 20, 50, 100];
  int _total = 100;
  int? _currentPerPage = 10;
  List<bool>? _expanded;
  int _currentPage = 1;
  bool _isSearch = false;
  final List<Map<String, dynamic>> _sourceOriginal = [];
  List<Map<String, dynamic>> _sourceFiltered = [];
  List<Map<String, dynamic>> _source = [];
  List<Map<String, dynamic>> _selecteds = [];
  String? _sortColumn;
  bool _sortAscending = true;

  final BehaviorSubject<bool> _isLoading = BehaviorSubject.seeded(true);

  final ScrollController scrollController = ScrollController();

  changeLoadingState(bool value) {
    _isLoading.sink.add(value);
  }
}
