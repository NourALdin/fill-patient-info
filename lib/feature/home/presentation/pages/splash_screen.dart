import 'package:flutter/material.dart';
import 'package:medical_project/core/config/theme/theme.dart';
import 'package:medical_project/feature/home/presentation/pages/home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 2)).then((value) {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => HomePage(),
      ));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Themes.dark.splashColor,
    );
  }
}
