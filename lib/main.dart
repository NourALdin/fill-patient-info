import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:isar/isar.dart';
import 'package:medical_project/core/config/theme/theme.dart';
import 'package:medical_project/core/feature/presentation/bloc/theme_bloc.dart';
import 'package:medical_project/core/services/service_locator.dart';
import 'package:medical_project/feature/home/presentation/pages/splash_screen.dart';
import 'package:medical_project/feature/patient/data/datasource/local_datasource/collections/patient_collection.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  _bootstrapApplication();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ThemeMode>(
        stream: locator<ThemeBloc>().getThemeModeStream,
        builder: (context, themeModeSnapshot) {
          return MaterialApp(
            title: 'Medical Application',
            debugShowCheckedModeBanner: false,
            themeMode: themeModeSnapshot.data,
            darkTheme: Themes.dark,
            theme: Themes.light,
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            locale: const Locale('ar'),
            supportedLocales: const [Locale('ar')],
            home: const SplashScreen(),
          );
        });
  }
}

_bootstrapApplication() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initDiContainer();
  final dir = await getApplicationDocumentsDirectory();
  print(dir.path);
  await Isar.open(
    [PatientSchema],
    directory: dir.path,
  ) /*.then((value) => value.copyToFile(r'C:\Users\NourAldin\Desktop'))*/;
}
