extension GetStringFromKeyElement on String {
  String getTitleFromTableKey() =>
      contains('_') ? split('_').reduce((value, element) => '${_firstLitterToUpper(value)} ${_firstLitterToUpper(element)}') : _firstLitterToUpper(this);
}

String _firstLitterToUpper(String s) => s[0].toUpperCase() + s.substring(1);
