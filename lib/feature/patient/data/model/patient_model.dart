import 'package:medical_project/core/feature/data/model/base_model.dart';
import 'package:medical_project/feature/patient/data/datasource/local_datasource/collections/patient_collection.dart';

class PatientModel extends BaseTableModel {
  final List<Patient> patients;

  PatientModel({required this.patients});

  @override
  List<String> get getTableKeys => ['id', 'name', 'phoneNumber', 'nextReview'];

  @override
  List<Map<String, dynamic>> toDataTable() {
    List<Map<String, dynamic>> temp = [];
    for (var element in patients) {
      temp.add(element.toJson());
    }
    return temp;
  }
}
