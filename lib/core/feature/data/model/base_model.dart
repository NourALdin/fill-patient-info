abstract class BaseTableModel {
  List<Map<String, dynamic>> toDataTable();

  List<String> get getTableKeys;
}
