import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:medical_project/core/feature/presentation/widgets/custom_stream_builder.dart';
import 'package:medical_project/core/services/navigation_service.dart';
import 'package:medical_project/feature/patient/data/datasource/local_datasource/collections/patient_collection.dart';
import 'package:medical_project/feature/patient/data/model/patient_model.dart';
import 'package:medical_project/feature/patient/presentations/pages/fill_in_patient_data.dart';
import 'package:medical_project/feature/patient/presentations/pages/patient_details_page.dart';
import 'package:medical_project/feature/patient/presentations/widgets/custom_table_widget.dart';
import 'package:rxdart/rxdart.dart';

class DisplayPatientsList extends StatefulWidget {
  const DisplayPatientsList({Key? key}) : super(key: key);

  @override
  State<DisplayPatientsList> createState() => _DisplayPatientsListState();
}

class _DisplayPatientsListState extends State<DisplayPatientsList> {
  final BehaviorSubject<PatientModel> patients = BehaviorSubject<PatientModel>();

  @override
  void initState() {
    getInfo(null);
    super.initState();
  }

  getInfo(String? query) async {
    final isar = Isar.getInstance();
    final p = await isar!.patients.filter().nameContains(query ?? '').findAll();
    patients.sink.add(PatientModel(patients: p));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: CustomStreamBuilder<PatientModel>(
            stream: patients.stream,
            onData: (dataSnapshot) {
              return CustomTableWidget<PatientModel>(
                data: dataSnapshot.data!,
                newEvent: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const FillInPatientData(),
                  ));
                },
                onTapRow: (data) {
                  NavigationService.push(context, PatientDetailsPage(patients: Patient.fromJson(data)));
                },
              );
            },
            onError: Container(),
          ),
        ),
      ),
    );
  }
}
