import 'package:flutter/material.dart';
import 'package:isar/isar.dart';
import 'package:medical_project/core/extensions/media_query.dart';
import 'package:medical_project/core/feature/presentation/widgets/custom_button.dart';
import 'package:medical_project/core/feature/presentation/widgets/custome_text_field.dart';
import 'package:medical_project/feature/patient/data/datasource/local_datasource/collections/patient_collection.dart';

import '../di_container/fill_patient_info_di.dart';

class FillInPatientData extends StatefulWidget {
  const FillInPatientData({Key? key, this.patients, this.fromPatientDetailsPage = false}) : super(key: key);

  final Patient? patients;
  final bool fromPatientDetailsPage;

  @override
  State<FillInPatientData> createState() => _FillInPatientDataState();
}

class _FillInPatientDataState extends State<FillInPatientData> with FillPatientInfoDi {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleTextStyle: const TextStyle(color: Colors.white, fontSize: 20),
        title: const Text('معلومات المريض'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: context.width * 0.3),
          child: Column(
            children: [
              CustomTextField(initialValue: widget.patients?.name, controller: nameController, label: 'اسم المريض', autofocus: true),
              CustomTextField(initialValue: widget.patients?.age.toString(), controller: ageController, label: 'العمر'),
              CustomTextField(initialValue: widget.patients?.phoneNumber, controller: phoneNumberController, label: 'الهاتف'),
              CustomTextField(initialValue: widget.patients?.numberOfChildren.toString(), controller: numberOfChildrenController, label: 'عدد الأولاد'),
              CustomTextField(initialValue: widget.patients?.firstChild, controller: firstChildController, label: 'الابن الأكبر'),
              CustomTextField(initialValue: widget.patients?.lastChild, controller: lastChildController, label: 'الابن الأصغر'),
              CustomTextField(initialValue: widget.patients?.medicalHistory, controller: medicalHistoryController, label: 'السوابق المرضية'),
              CustomTextField(initialValue: widget.patients?.medicationsUsed, controller: medicationsUsedController, label: 'الأدوية المستخدمة'),
              CustomTextField(initialValue: widget.patients?.previousOperations, controller: previousOperationsController, label: 'العمليات السابقة'),
              CustomTextField(initialValue: widget.patients?.mainComplaint, controller: mainComplaintController, label: 'الشكوى الرئيسية'),
              CustomTextField(initialValue: widget.patients?.clinicalExamination, controller: clinicalExaminationController, label: 'الفحص السريري'),
              CustomTextField(initialValue: widget.patients?.muscularStrength, controller: muscularStrengthController, label: 'القوة العضلية'),
              CustomTextField(initialValue: widget.patients?.neurologicalExamination, controller: neurologicalExaminationController, label: 'الفحص العصبي'),
              CustomTextField(initialValue: widget.patients?.requiredAnalyzes, controller: requiredAnalyzesController, label: 'التحاليل المطلوبة'),
              CustomTextField(initialValue: widget.patients?.radiographic, controller: radiographicController, label: 'نتائج الصورة الشعاعية'),
              CustomTextField(initialValue: widget.patients?.notes, controller: notesController, label: 'ملاحظات'),
              CustomTextField(initialValue: widget.patients?.prescribedMedications, controller: prescribedMedicationsController, label: 'الأدوية الموصوفة'),
              DatePickerDialog(initialDate: DateTime.now(), firstDate: DateTime.now(), lastDate: DateTime(3000)),
              CustomTextField(initialValue: widget.patients?.nextReview.toString(), controller: nextReviewController, label: 'المراجعة القادمة'),
              CustomButton(
                  onTap: () async {
                    final isar = Isar.getInstance();
                    final newPatient = Patient(
                      name: nameController.value.text,
                      age: int.parse(ageController.value.text),
                      phoneNumber: phoneNumberController.value.text,
                      numberOfChildren: int.parse(numberOfChildrenController.value.text),
                      firstChild: firstChildController.value.text,
                      lastChild: lastChildController.value.text,
                      medicalHistory: medicalHistoryController.value.text,
                      medicationsUsed: medicationsUsedController.value.text,
                      previousOperations: previousOperationsController.value.text,
                      mainComplaint: mainComplaintController.value.text,
                      clinicalExamination: clinicalExaminationController.value.text,
                      muscularStrength: muscularStrengthController.value.text,
                      neurologicalExamination: neurologicalExaminationController.value.text,
                      requiredAnalyzes: requiredAnalyzesController.value.text,
                      radiographic: radiographicController.value.text,
                      notes: notesController.value.text,
                      prescribedMedications: prescribedMedicationsController.value.text,
                      nextReview: DateTime.now(),
                    );
                    if (widget.fromPatientDetailsPage) {
                      await isar!.writeTxn(() async {
                        newPatient.id = widget.patients!.id;
                        final patient = await isar.patients.get(widget.patients!.id!);

                        patient!.copyWith(phoneNumber: phoneNumberController.value.text);

                        await isar.patients.put(patient);
                        print(newPatient.id);
                      }).then((value) => print('-----------------------------------$value'));
                    } else {
                      await isar!.writeTxn(() async {
                        await isar.patients.put(newPatient);
                      }).catchError((e) {
                        print(e);
                      }).then((value) => print(value));
                    }
                  },
                  text: widget.fromPatientDetailsPage ? 'تحديث المعلومات' : 'إضافة'),
            ],
          ),
        ),
      ),
    );
  }
}
