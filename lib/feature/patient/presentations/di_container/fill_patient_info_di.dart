import 'package:flutter/material.dart';

mixin FillPatientInfoDi {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController ageController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController numberOfChildrenController = TextEditingController();
  final TextEditingController firstChildController = TextEditingController();
  final TextEditingController lastChildController = TextEditingController();
  final TextEditingController medicalHistoryController = TextEditingController();
  final TextEditingController medicationsUsedController = TextEditingController();
  final TextEditingController previousOperationsController = TextEditingController();
  final TextEditingController mainComplaintController = TextEditingController();
  final TextEditingController clinicalExaminationController = TextEditingController();
  final TextEditingController muscularStrengthController = TextEditingController();
  final TextEditingController neurologicalExaminationController = TextEditingController();
  final TextEditingController requiredAnalyzesController = TextEditingController();
  final TextEditingController radiographicController = TextEditingController();
  final TextEditingController notesController = TextEditingController();
  final TextEditingController prescribedMedicationsController = TextEditingController();
  final TextEditingController nextReviewController = TextEditingController();
}
