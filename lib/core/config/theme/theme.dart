import 'package:flutter/material.dart';
import 'package:medical_project/core/config/theme/app_colors.dart';

class Themes {
  static final light = ThemeData.light().copyWith(
      useMaterial3: true,
      primaryColor: AppColors.primary,
      primaryColorLight: Colors.blue[400],
      primaryColorDark: Colors.blue[700],
      iconTheme: const IconThemeData(color: Colors.black54),
      bannerTheme: MaterialBannerThemeData(
        backgroundColor: Colors.blue.shade500,
      ),
      appBarTheme: const AppBarTheme(
        backgroundColor: Colors.blue,
      ),
      iconButtonTheme: IconButtonThemeData(
          style: ButtonStyle(
        iconColor: MaterialStateProperty.all(const Color(0xff303030)),
      )),
      bottomSheetTheme: const BottomSheetThemeData(
        backgroundColor: Colors.white,
      ),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(backgroundColor: Colors.blue),
      splashColor: Colors.lightBlueAccent,
      inputDecorationTheme: InputDecorationTheme(labelStyle: TextStyle(color: Colors.black)));

  static final dark = ThemeData.dark().copyWith(
    useMaterial3: true,
    primaryColor: Colors.blue,
    primaryColorDark: Colors.blue[700],
    iconTheme: const IconThemeData(color: Colors.white),
    bannerTheme: MaterialBannerThemeData(
      backgroundColor: Colors.amber.shade900.withAlpha(200),
    ),
    bottomSheetTheme: BottomSheetThemeData(
      backgroundColor: Colors.grey[900],
    ),
    appBarTheme: AppBarTheme(
      backgroundColor: Colors.grey[800],
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(backgroundColor: Colors.blue),
    scaffoldBackgroundColor: const Color(0xff303030),
    splashColor: const Color(0xff303030),
  );
}
