import 'package:flutter/material.dart';
import 'package:medical_project/feature/patient/data/datasource/local_datasource/collections/patient_collection.dart';
import 'package:medical_project/feature/patient/presentations/pages/fill_in_patient_data.dart';

class PatientDetailsPage extends StatelessWidget {
  const PatientDetailsPage({Key? key, required this.patients}) : super(key: key);

  final Patient patients;

  @override
  Widget build(BuildContext context) => FillInPatientData(
        patients: patients,
        fromPatientDetailsPage: true,
      );
}
