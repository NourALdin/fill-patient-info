import 'package:isar/isar.dart';

part 'patient_collection.g.dart';

@collection
class Patient {
  Id? id = Isar.autoIncrement; // you can also use id = null to auto increment
  String? name;
  int? age;
  String? phoneNumber;
  int? numberOfChildren;
  String? firstChild;
  String? lastChild;
  String? medicalHistory;
  String? medicationsUsed;
  String? previousOperations;
  String? mainComplaint;
  String? clinicalExamination;
  String? muscularStrength;
  String? neurologicalExamination;
  String? requiredAnalyzes;
  String? radiographic;
  String? notes;
  String? prescribedMedications;
  DateTime? nextReview;

  Patient(
      {this.id,
      this.name,
      this.phoneNumber,
      this.numberOfChildren,
      this.firstChild,
      this.lastChild,
      this.medicalHistory,
      this.medicationsUsed,
      this.previousOperations,
      this.mainComplaint,
      this.clinicalExamination,
      this.muscularStrength,
      this.requiredAnalyzes,
      this.radiographic,
      this.notes,
      this.prescribedMedications,
      this.nextReview,
      this.age,
      this.neurologicalExamination});

  factory Patient.fromJson(Map<String, dynamic> json) {
    return Patient(
      id: json['id'],
      name: json['name'],
      age: json['age'],
      phoneNumber: json['phoneNumber'],
      numberOfChildren: json['numberOfChildren'],
      firstChild: json['firstChild'],
      lastChild: json['lastChild'],
      medicalHistory: json['medicalHistory'],
      medicationsUsed: json['medicationsUsed'],
      previousOperations: json['previousOperations'],
      mainComplaint: json['mainComplaint'],
      clinicalExamination: json['clinicalExamination'],
      muscularStrength: json['muscularStrength'],
      neurologicalExamination: json['neurologicalExamination'],
      requiredAnalyzes: json['requiredAnalyzes'],
      radiographic: json['radiographic'],
      notes: json['notes'],
      prescribedMedications: json['prescribedMedications'],
      nextReview: DateTime.tryParse(json['nextReview']),
    );
  }

  Patient copyWith({
    String? name,
    int? age,
    String? phoneNumber,
    int? numberOfChildren,
    String? firstChild,
    String? lastChild,
    String? medicalHistory,
    String? medicationsUsed,
    String? previousOperations,
    String? mainComplaint,
    String? clinicalExamination,
    String? muscularStrength,
    String? neurologicalExamination,
    String? requiredAnalyzes,
    String? radiographic,
    String? notes,
    String? prescribedMedications,
    DateTime? nextReview,
  }) {
    return Patient(
      name: name ?? this.name,
      age: age ?? this.age,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      numberOfChildren: numberOfChildren ?? this.numberOfChildren,
      firstChild: firstChild ?? this.firstChild,
      lastChild: lastChild ?? this.lastChild,
      medicalHistory: medicalHistory ?? this.medicalHistory,
      medicationsUsed: medicationsUsed ?? this.medicationsUsed,
      previousOperations: previousOperations ?? this.previousOperations,
      mainComplaint: mainComplaint ?? this.mainComplaint,
      clinicalExamination: clinicalExamination ?? this.clinicalExamination,
      muscularStrength: muscularStrength ?? this.muscularStrength,
      neurologicalExamination: neurologicalExamination ?? this.neurologicalExamination,
      requiredAnalyzes: requiredAnalyzes ?? this.requiredAnalyzes,
      radiographic: radiographic ?? this.radiographic,
      notes: notes ?? this.notes,
      prescribedMedications: prescribedMedications ?? this.prescribedMedications,
      nextReview: nextReview ?? this.nextReview,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'age': age,
        'phoneNumber': phoneNumber,
        'numberOfChildren': numberOfChildren,
        'firstChild': firstChild,
        'lastChild': lastChild,
        'medicalHistory': medicalHistory,
        'medicationsUsed': medicationsUsed,
        'previousOperations': previousOperations,
        'mainComplaint': mainComplaint,
        'clinicalExamination': clinicalExamination,
        'muscularStrength': muscularStrength,
        'neurologicalExamination': neurologicalExamination,
        'requiredAnalyzes': requiredAnalyzes,
        'radiographic': radiographic,
        'notes': notes,
        'prescribedMedications': prescribedMedications,
        'nextReview': nextReview?.toIso8601String(),
      };
}
