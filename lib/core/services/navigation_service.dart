import 'package:flutter/material.dart';

class NavigationService {
  static push(BuildContext context, page) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => page));
  }
}
