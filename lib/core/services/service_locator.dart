import 'package:get_it/get_it.dart';
import 'package:medical_project/core/feature/presentation/bloc/theme_bloc.dart';
import 'package:medical_project/feature/home/presentation/bloc/home_page_bloc.dart';

final locator = GetIt.instance;

initDiContainer() {
  locator.registerSingleton(ThemeBloc());
  locator.registerSingleton(HomeBloc());
}
