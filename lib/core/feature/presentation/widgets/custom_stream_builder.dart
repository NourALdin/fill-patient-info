import 'package:flutter/material.dart';

class CustomStreamBuilder<T> extends StreamBuilder<T> {
  final Widget Function(AsyncSnapshot<T> snapshot) onData;
  final Widget onError;

  CustomStreamBuilder({super.key, super.stream, required this.onData, required this.onError})
      : super(builder: (context, snapshot) {
          if (snapshot.hasData) {
            return onData(snapshot);
          } else {
            return onError;
          }
        });
}
