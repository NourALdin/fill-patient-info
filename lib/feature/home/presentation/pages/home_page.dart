import 'package:flutter/material.dart';
import 'package:medical_project/core/feature/presentation/bloc/theme_bloc.dart';
import 'package:medical_project/core/services/service_locator.dart';
import 'package:medical_project/feature/patient/presentations/pages/display_patients_list.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  final ThemeBloc themeBloc = locator<ThemeBloc>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () => themeBloc.changeThemeMode(),
            icon: Icon(themeBloc.getThemeModeValue == ThemeMode.dark ? Icons.circle_outlined : Icons.circle),
          )
        ],
      ),
      body: const Center(
        child: DisplayPatientsList(),
      ),
    );
  }
}
